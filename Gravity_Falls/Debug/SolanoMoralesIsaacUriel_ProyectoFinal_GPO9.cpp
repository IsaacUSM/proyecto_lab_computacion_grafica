﻿#include <iostream>
#include <cmath>
#include<sstream>
#include<fstream>
// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>
// Other Libs
#include "stb_image.h"
// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
//Load Models
#include "SOIL2/SOIL2.h"
// Other includes
#include "Shader.h"
#include "Camera.h"
#include "Model.h"
#include "Texture.h"

// Function prototypes
void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);
void MouseCallback(GLFWwindow *window, double xPos, double yPos);
void DoMovement();
void animacion();
void animacion2();
void animacionC();
void animacionCrist();
void animacionNave();
void animacionCharola();
void animacionTapaCofre();
void cargaFrames();
void cargaFrames2();
// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;
int SCREEN_WIDTH, SCREEN_HEIGHT;
// Camera
Camera  camera(glm::vec3(0.0f, 10.0f, 0.0f));
GLfloat lastX = WIDTH / 2.0;
GLfloat lastY = HEIGHT / 2.0;
bool keys[1024];
bool firstMouse = true;
float range = 0.0f;
float rot = 0.0f;
// Light attributes
glm::vec3 lightPos(0.0f, 0.0f, 0.0f);
glm::vec3 PosIni(0.0f, 0.0f, 0.0f);
glm::vec3 PosIni2(0.0f, 0.0f, 0.0f);
glm::vec3 PosIniCarro(-10.0f, 0.0f, -10.0f);
glm::vec3 PosIniCristal1(-2.521f, 2.27f, -0.755f);
glm::vec3 PosIniCristal2(-2.521f, 2.27f, -0.241f);
glm::vec3 PosIniNave(30.0f, 30.0f, -15.0f);
glm::vec3 PosIniCharola(-2.6f, 2.434f, -1.398f);
glm::vec3 PosIniTapaCofre(-2.16f, 2.2f, -5.5f);
bool active;
// Deltatime
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame
// Keyframes
float posX = PosIni.x, posY = PosIni.y, posZ = PosIni.z,
rotRodIzqX = 0, rotRodDerX = 0, rotBraIzqX = 0, rotBraDerX = 0, rotMunDerX = 0, rotMunIzqX = 0,
rotPieIzqX = 0, rotPieDerX = 0, rotBraIzqZ = 0, rotBraDerZ = 0, rotMunDerZ = 0, rotMunIzqZ = 0;
float pos2X = PosIni2.x, pos2Y = PosIni2.y, pos2Z = PosIni2.z,
rotBraIzq2X = 0, rotBraDer2X = 0, rotMunDer2X = 0, rotMunIzq2X = 0,
rotBraIzq2Z = 0, rotBraDer2Z = 0, rotMunDer2Z = 0, rotMunIzq2Z = 0;

#define MAX_FRAMES 11
int i_max_steps = 190;
int i_curr_steps = 0;
#define MAX_FRAMES2 12
int i_max_steps2 = 190;
int i_curr_steps2 = 0;

//Estructura de los frames para la animacion del personaje tio
typedef struct _frame
{
	//Variables para GUARDAR Key Frames
	float posX;		//Variable para PosicionX
	float posY;		//Variable para PosicionY
	float posZ;		//Variable para PosicionZ
	float incX;		//Variable para IncrementoX
	float incY;		//Variable para IncrementoY
	float incZ;		//Variable para IncrementoZ

	float rotRodIzqX;
	float rotRodDerX;
	float rotBraIzqX;
	float rotBraDerX;
	float rotMunDerX;
	float rotMunIzqX;
	float rotPieIzqX;
	float rotPieDerX;

	float rotBraIzqZ;
	float rotBraDerZ;
	float rotMunDerZ;
	float rotMunIzqZ;

	float rotIncRodIzqX;
	float rotIncRodDerX;
	float rotIncBraIzqX;
	float rotIncBraDerX;
	float rotIncMunDerX;
	float rotIncMunIzqX;
	float rotIncPieIzqX;
	float rotIncPieDerX;

	float rotIncBraIzqZ;
	float rotIncBraDerZ;
	float rotIncMunDerZ;
	float rotIncMunIzqZ;
}FRAME;

//Estructura de los frames para la animacion del personaje Wendy
typedef struct _frame2
{
	//Variables para GUARDAR Key Frames
	float pos2X;		//Variable para PosicionX
	float pos2Y;		//Variable para PosicionY
	float pos2Z;		//Variable para PosicionZ
	float inc2X;		//Variable para IncrementoX
	float inc2Y;		//Variable para IncrementoY
	float inc2Z;		//Variable para IncrementoZ

	float rotBraIzq2X;
	float rotBraDer2X;
	float rotMunDer2X;
	float rotMunIzq2X;

	float rotBraIzq2Z;
	float rotBraDer2Z;
	float rotMunDer2Z;
	float rotMunIzq2Z;

	float rotIncBraIzq2X;
	float rotIncBraDer2X;
	float rotIncMunDer2X;
	float rotIncMunIzq2X;

	float rotIncBraIzq2Z;
	float rotIncBraDer2Z;
	float rotIncMunDer2Z;
	float rotIncMunIzq2Z;
}FRAME2;

FRAME KeyFrame[MAX_FRAMES];
FRAME2 KeyFrame2[MAX_FRAMES2];
int FrameIndex = 0;			//introducir datos
bool play = false;
int playIndex = 0;

int FrameIndex2 = 0;			//introducir datos
bool play2 = false;
int playIndex2 = 0;

// Positions of the point lights
glm::vec3 pointLightPositions[] = {
	glm::vec3(0.0f,0.0f,0.0f),
	glm::vec3(-3.75f,4.4f,4.45f),
	glm::vec3(0.0f,0.0f,0.0f),
	glm::vec3(0.0f,0.0f,0.0f)
};

//Variables de ayuda para las animaciones
glm::vec3 LightP1;
float movCarroX = 0.0;
float movCarroZ = 0.0;
float rotIniCarro = 45.0;
float rotCarro = 0.0;
float movCristZ1 = 0.0;
float movCristZ2 = 0.0;
float movNaveX = 0.0;
float movNaveY = 0.0;
float movNaveZ = 0.0;
float rotNave = 0.0;
float movCharolaX = 0.0;
float rotTapaCofre = 0.0;
bool animTapaCofre = false;
bool recoTapaCofre1 = true;
bool recoTapaCofre2 = false;
bool animCharola = false;
bool recoCharola1 = true;
bool recoCharola2 = false;
bool animNave = false;
bool recoNave1 = true;
bool recoNave2 = false;
bool recoNave3 = false;
bool recoNave4 = false;
bool recoNave5 = false;
bool recoNave6 = false;
bool animCristal = false;
bool recoCrist1 = true;
bool recoCrist2 = false;
bool recoCrist3 = false;
bool recoCrist4 = false;
bool animCarro = false;
bool recorrido1 = true;
bool recorrido2 = false;
bool recorrido3 = false;
bool recorrido4 = false;

//Lee del archivo los valores para cargarlos en el arreglo de keyframes para la animacion del personaje Tio
void cargaFrames(void){
	int FrameIndex1 = 0;
	ifstream archivo("FramesTio.csv");
	string linea;
	char delimitador = ',';
	// Leemos la primer l�nea para descartarla, pues es el encabezado
	getline(archivo, linea);
	// Leemos todas las l�neas
	while (getline(archivo, linea))
	{
		stringstream lineaLeida(linea); // Convertir la cadena a un stream
		string posX, posY, posZ,
		rotRodIzqX, rotRodDerX, rotBraIzqX, rotBraDerX, rotMunDerX, rotMunIzqX, rotPieIzqX, rotPieDerX, rotBraIzqZ, rotBraDerZ, rotMunDerZ, rotMunIzqZ;
		// Extraer todos los valores de esa fila
		getline(lineaLeida, posX, delimitador);
		getline(lineaLeida, posY, delimitador);
		getline(lineaLeida, posZ, delimitador);

		getline(lineaLeida, rotRodIzqX, delimitador);
		getline(lineaLeida, rotRodDerX, delimitador);
		getline(lineaLeida, rotBraIzqX, delimitador);
		getline(lineaLeida, rotBraDerX, delimitador);
		getline(lineaLeida, rotMunDerX, delimitador);
		getline(lineaLeida, rotMunIzqX, delimitador);
		getline(lineaLeida, rotPieIzqX, delimitador);
		getline(lineaLeida, rotPieDerX, delimitador);

		getline(lineaLeida, rotBraIzqZ, delimitador);
		getline(lineaLeida, rotBraDerZ, delimitador);
		getline(lineaLeida, rotMunDerZ, delimitador);
		getline(lineaLeida, rotMunIzqZ, delimitador);

		KeyFrame[FrameIndex1].posX = std::stof(posX);
		KeyFrame[FrameIndex1].posY = std::stof(posY);
		KeyFrame[FrameIndex1].posZ = std::stof(posZ);

		KeyFrame[FrameIndex1].rotRodIzqX = std::stof(rotRodIzqX);
		KeyFrame[FrameIndex1].rotRodDerX = std::stof(rotRodDerX);
		KeyFrame[FrameIndex1].rotPieIzqX = std::stof(rotPieIzqX);
		KeyFrame[FrameIndex1].rotPieDerX = std::stof(rotPieDerX);
		KeyFrame[FrameIndex1].rotBraIzqX = std::stof(rotBraIzqX);
		KeyFrame[FrameIndex1].rotBraDerX = std::stof(rotBraDerX);
		KeyFrame[FrameIndex1].rotMunIzqX = std::stof(rotMunIzqX);
		KeyFrame[FrameIndex1].rotMunDerX = std::stof(rotMunDerX);

		KeyFrame[FrameIndex1].rotBraIzqZ = std::stof(rotBraIzqZ);
		KeyFrame[FrameIndex1].rotBraDerZ = std::stof(rotBraDerZ);
		KeyFrame[FrameIndex1].rotMunDerZ = std::stof(rotMunDerZ);
		KeyFrame[FrameIndex1].rotMunIzqZ = std::stof(rotMunIzqZ);
		printf("\nFrame %d cargado con exito", FrameIndex1);
		FrameIndex = FrameIndex1;
		FrameIndex1++;
	}
	archivo.close();
	printf("\nFrames cargados con exito");
}

//Lee del archivo los valores para cargarlos en el arreglo de keyframes para la animacion del personaje Wendy
void cargaFrames2(void) {
	int FrameIndex1 = 0;
	ifstream archivo("FramesWendy.csv");
	string linea;
	char delimitador = ',';
	// Leemos la primer l�nea para descartarla, pues es el encabezado
	getline(archivo, linea);
	// Leemos todas las l�neas
	while (getline(archivo, linea))
	{
		stringstream lineaLeida(linea); // Convertir la cadena a un stream
		string pos2X, pos2Y, pos2Z,rotBraIzq2X, rotBraDer2X, rotMunDer2X, rotMunIzq2X, rotBraIzq2Z, rotBraDer2Z, rotMunDer2Z, rotMunIzq2Z;
		// Extraer todos los valores de esa fila
		getline(lineaLeida, pos2X, delimitador);
		getline(lineaLeida, pos2Y, delimitador);
		getline(lineaLeida, pos2Z, delimitador);

		getline(lineaLeida, rotBraIzq2X, delimitador);
		getline(lineaLeida, rotBraDer2X, delimitador);
		getline(lineaLeida, rotMunDer2X, delimitador);
		getline(lineaLeida, rotMunIzq2X, delimitador);

		getline(lineaLeida, rotBraIzq2Z, delimitador);
		getline(lineaLeida, rotBraDer2Z, delimitador);
		getline(lineaLeida, rotMunDer2Z, delimitador);
		getline(lineaLeida, rotMunIzq2Z, delimitador);

		KeyFrame2[FrameIndex2].pos2X = std::stof(pos2X);
		KeyFrame2[FrameIndex2].pos2Y = std::stof(pos2Y);
		KeyFrame2[FrameIndex2].pos2Z = std::stof(pos2Z);

		KeyFrame2[FrameIndex2].rotBraIzq2X = std::stof(rotBraIzq2X);
		KeyFrame2[FrameIndex2].rotBraDer2X = std::stof(rotBraDer2X);
		KeyFrame2[FrameIndex2].rotMunIzq2X = std::stof(rotMunIzq2X);
		KeyFrame2[FrameIndex2].rotMunDer2X = std::stof(rotMunDer2X);

		KeyFrame2[FrameIndex2].rotBraIzq2Z = std::stof(rotBraIzq2Z);
		KeyFrame2[FrameIndex2].rotBraDer2Z = std::stof(rotBraDer2Z);
		KeyFrame2[FrameIndex2].rotMunDer2Z = std::stof(rotMunDer2Z);
		KeyFrame2[FrameIndex2].rotMunIzq2Z = std::stof(rotMunIzq2Z);
		printf("\nFrame %d cargado con exito", FrameIndex1);
		FrameIndex2 = FrameIndex1;
		FrameIndex1++;
	}
	archivo.close();
	printf("\nFrames cargados con exito");
}

//Funcion que reinicia los valores de la animacion para el personaje Tio
void resetElements(void)
{
	posX = KeyFrame[0].posX;
	posY = KeyFrame[0].posY;
	posZ = KeyFrame[0].posZ;

	rotRodIzqX = KeyFrame[0].rotRodIzqX;
	rotRodDerX = KeyFrame[0].rotRodDerX;
	rotPieIzqX = KeyFrame[0].rotPieIzqX;
	rotPieDerX = KeyFrame[0].rotPieDerX;
	rotBraIzqX = KeyFrame[0].rotBraIzqX;
	rotBraDerX = KeyFrame[0].rotBraDerX;
	rotMunIzqX = KeyFrame[0].rotMunIzqX;
	rotMunDerX = KeyFrame[0].rotMunDerX;

	rotBraIzqZ = KeyFrame[0].rotBraIzqZ;
	rotBraDerZ = KeyFrame[0].rotBraDerZ;
	rotMunIzqZ = KeyFrame[0].rotMunIzqZ;
	rotMunDerZ = KeyFrame[0].rotMunDerZ;
}

//Funcion que reinicia los valores de la animacion para el personaje Wendy
void resetElements2(void)
{
	pos2X = KeyFrame2[0].pos2X;
	pos2Y = KeyFrame2[0].pos2Y;
	pos2Z = KeyFrame2[0].pos2Z;

	rotBraIzq2X = KeyFrame2[0].rotBraIzq2X;
	rotBraDer2X = KeyFrame2[0].rotBraDer2X;
	rotMunIzq2X = KeyFrame2[0].rotMunIzq2X;
	rotMunDer2X = KeyFrame2[0].rotMunDer2X;

	rotBraIzq2Z = KeyFrame2[0].rotBraIzq2Z;
	rotBraDer2Z = KeyFrame2[0].rotBraDer2Z;
	rotMunIzq2Z = KeyFrame2[0].rotMunIzq2Z;
	rotMunDer2Z = KeyFrame2[0].rotMunDer2Z;
}

//Funcion que realiza los incrementos para reproducir la animacion por Keyframes del personaje Tio
void interpolation(void)
{
	KeyFrame[playIndex].incX = (KeyFrame[playIndex + 1].posX - KeyFrame[playIndex].posX) / i_max_steps;
	KeyFrame[playIndex].incY = (KeyFrame[playIndex + 1].posY - KeyFrame[playIndex].posY) / i_max_steps;
	KeyFrame[playIndex].incZ = (KeyFrame[playIndex + 1].posZ - KeyFrame[playIndex].posZ) / i_max_steps;

	KeyFrame[playIndex].rotIncRodIzqX = (KeyFrame[playIndex + 1].rotRodIzqX - KeyFrame[playIndex].rotRodIzqX) / i_max_steps;
	KeyFrame[playIndex].rotIncRodDerX = (KeyFrame[playIndex + 1].rotRodDerX - KeyFrame[playIndex].rotRodDerX) / i_max_steps;
	KeyFrame[playIndex].rotIncPieIzqX = (KeyFrame[playIndex + 1].rotPieIzqX - KeyFrame[playIndex].rotPieIzqX) / i_max_steps;
	KeyFrame[playIndex].rotIncPieDerX = (KeyFrame[playIndex + 1].rotPieDerX - KeyFrame[playIndex].rotPieDerX) / i_max_steps;
	KeyFrame[playIndex].rotIncBraIzqX = (KeyFrame[playIndex + 1].rotBraIzqX - KeyFrame[playIndex].rotBraIzqX) / i_max_steps;
	KeyFrame[playIndex].rotIncBraDerX = (KeyFrame[playIndex + 1].rotBraDerX - KeyFrame[playIndex].rotBraDerX) / i_max_steps;
	KeyFrame[playIndex].rotIncMunIzqX = (KeyFrame[playIndex + 1].rotMunIzqX - KeyFrame[playIndex].rotMunIzqX) / i_max_steps;
	KeyFrame[playIndex].rotIncMunDerX = (KeyFrame[playIndex + 1].rotMunDerX - KeyFrame[playIndex].rotMunDerX) / i_max_steps;

	KeyFrame[playIndex].rotIncBraIzqZ = (KeyFrame[playIndex + 1].rotBraIzqZ - KeyFrame[playIndex].rotBraIzqZ) / i_max_steps;
	KeyFrame[playIndex].rotIncBraDerZ = (KeyFrame[playIndex + 1].rotBraDerZ - KeyFrame[playIndex].rotBraDerZ) / i_max_steps;
	KeyFrame[playIndex].rotIncMunIzqZ = (KeyFrame[playIndex + 1].rotMunIzqZ - KeyFrame[playIndex].rotMunIzqZ) / i_max_steps;
	KeyFrame[playIndex].rotIncMunDerZ = (KeyFrame[playIndex + 1].rotMunDerZ - KeyFrame[playIndex].rotMunDerZ) / i_max_steps;
}

//Funcion que realiza los incrementos para reproducir la animacion por Keyframes del personaje Wendy
void interpolation2(void)
{
	KeyFrame2[playIndex2].inc2X = (KeyFrame2[playIndex2 + 1].pos2X - KeyFrame2[playIndex2].pos2X) / i_max_steps2;
	KeyFrame2[playIndex2].inc2Y = (KeyFrame2[playIndex2 + 1].pos2Y - KeyFrame2[playIndex2].pos2Y) / i_max_steps2;
	KeyFrame2[playIndex2].inc2Z = (KeyFrame2[playIndex2 + 1].pos2Z - KeyFrame2[playIndex2].pos2Z) / i_max_steps2;

	KeyFrame2[playIndex2].rotIncBraIzq2X = (KeyFrame2[playIndex2 + 1].rotBraIzq2X - KeyFrame2[playIndex2].rotBraIzq2X) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncBraDer2X = (KeyFrame2[playIndex2 + 1].rotBraDer2X - KeyFrame2[playIndex2].rotBraDer2X) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncMunIzq2X = (KeyFrame2[playIndex2 + 1].rotMunIzq2X - KeyFrame2[playIndex2].rotMunIzq2X) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncMunDer2X = (KeyFrame2[playIndex2 + 1].rotMunDer2X - KeyFrame2[playIndex2].rotMunDer2X) / i_max_steps2;

	KeyFrame2[playIndex2].rotIncBraIzq2Z = (KeyFrame2[playIndex2 + 1].rotBraIzq2Z - KeyFrame2[playIndex2].rotBraIzq2Z) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncBraDer2Z = (KeyFrame2[playIndex2 + 1].rotBraDer2Z - KeyFrame2[playIndex2].rotBraDer2Z) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncMunIzq2Z = (KeyFrame2[playIndex2 + 1].rotMunIzq2Z - KeyFrame2[playIndex2].rotMunIzq2Z) / i_max_steps2;
	KeyFrame2[playIndex2].rotIncMunDer2Z = (KeyFrame2[playIndex2 + 1].rotMunDer2Z - KeyFrame2[playIndex2].rotMunDer2Z) / i_max_steps2;
}

int main()
{
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	/*(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);*/
	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Proyecto Final", nullptr, nullptr);
	if (nullptr == window)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();

		return EXIT_FAILURE;
	}
	glfwMakeContextCurrent(window);
	glfwGetFramebufferSize(window, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	// Set the required callback functions
	glfwSetKeyCallback(window, KeyCallback);
	glfwSetCursorPosCallback(window, MouseCallback);
	printf("%f", glfwGetTime());
	// GLFW Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (GLEW_OK != glewInit())
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return EXIT_FAILURE;
	}
	// Define the viewport dimensions
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	// OpenGL options
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//Carga de todos los modelos-------------------------------------------------------------------------------------------------------
	Shader lightingShader("Shaders/lighting.vs", "Shaders/lighting.frag");
	Shader lampShader("Shaders/lamp.vs", "Shaders/lamp.frag");
	Shader SkyBoxshader("Shaders/SkyBox.vs", "Shaders/SkyBox.frag");
	Model cabana((char*)"Models/cabana.obj");
	Model wendy((char*)"Models/wendy.obj");
	Model braIzqWendy((char*)"Models/brazoIzqwendy.obj");
	Model braDerWendy((char*)"Models/brazoDerwendy.obj");
	Model munIzqWendy((char*)"Models/munecaIzqwendy.obj");
	Model munDerWendy((char*)"Models/munecaDerwendy.obj");
	Model waddles((char*)"Models/waddles.obj");
	Model tio((char*)"Models/tio.obj");
	Model brazoIzqTio((char*)"Models/braIzqTio.obj");
	Model brazoDerTio((char*)"Models/braDerTio.obj");
	Model munecaIzqTio((char*)"Models/munecaIzqTio.obj");
	Model munecaDerTio((char*)"Models/munecaDerTio.obj");
	Model piernaIzqTio((char*)"Models/pierIzqTio.obj");
	Model piernaDerTio((char*)"Models/pierDerTio.obj");
	Model pieIzqTio((char*)"Models/pieIzqTio.obj");
	Model pieDerTio((char*)"Models/pieDerTio.obj");
	Model suelo((char*)"Models/suelof.obj");
	Model nave1((char*)"Models/nave.obj");
	Model lampara((char*)"Models/lampara.obj");
	Model dipper((char*)"Models/dipper.obj");
	Model mabel((char*)"Models/mabel.obj");
	Model bill((char*)"Models/bill.obj");
	Model maquina((char*)"Models/maquina.obj");
	Model pie((char*)"Models/pie.obj");
	Model casco((char*)"Models/casco.obj");
	Model tumbas((char*)"Models/tumbas.obj");
	Model nave2((char*)"Models/nave2.obj");
	Model nave3((char*)"Models/nave3.obj");
	Model cuadernos((char*)"Models/cuadernos.obj");
	Model cubo((char*)"Models/cubo.obj");
	Model escritorio((char*)"Models/escritorio.obj");
	Model pez((char*)"Models/pez.obj");
	Model reloj((char*)"Models/reloj.obj");
	Model unicornio((char*)"Models/unicornio.obj");
	Model chetos((char*)"Models/chetos.obj");
	Model silla((char*)"Models/silla.obj");
	Model alfombra((char*)"Models/alfombra.obj");
	Model helado((char*)"Models/heladera.obj");
	Model cristal1((char*)"Models/crisDerHe.obj");
	Model cristal2((char*)"Models/CrisIzqHe.obj");
	Model carrito((char*)"Models/carrito.obj");
	Model barril((char*)"Models/barriles.obj");
	Model mueble((char*)"Models/mueble.obj");
	Model registradora((char*)"Models/registradora.obj");
	Model charola((char*)"Models/charolaRegis.obj");
	Model cofre((char*)"Models/cofre.obj");
	Model tapaCofre((char*)"Models/tapaCofre.obj");
	// Build and compile our shader program
	//Inicialización de KeyFrames
	for (int i = 0; i < MAX_FRAMES; i++)
	{
		KeyFrame[i].posX = 0;
		KeyFrame[i].incX = 0;
		KeyFrame[i].incY = 0;
		KeyFrame[i].incZ = 0;
		KeyFrame[i].rotRodIzqX = 0,
		KeyFrame[i].rotRodDerX = 0;
		KeyFrame[i].rotBraIzqX = 0;
		KeyFrame[i].rotBraDerX = 0;
		KeyFrame[i].rotMunDerX = 0;
		KeyFrame[i].rotMunIzqX = 0;
		KeyFrame[i].rotPieIzqX = 0;
		KeyFrame[i].rotPieDerX = 0;
		KeyFrame[i].rotBraIzqZ = 0;
		KeyFrame[i].rotBraDerZ = 0;
		KeyFrame[i].rotMunDerZ = 0;
		KeyFrame[i].rotMunIzqZ = 0;
		KeyFrame2[i].pos2X = 0;
		KeyFrame2[i].inc2X = 0;
		KeyFrame2[i].inc2Y = 0;
		KeyFrame2[i].inc2Z = 0;
		KeyFrame2[i].rotBraIzq2X = 0;
		KeyFrame2[i].rotBraDer2X = 0;
		KeyFrame2[i].rotMunDer2X = 0;
		KeyFrame2[i].rotMunIzq2X = 0;
		KeyFrame2[i].rotBraIzq2Z = 0;
		KeyFrame2[i].rotBraDer2Z = 0;
		KeyFrame2[i].rotMunDer2Z = 0;
		KeyFrame2[i].rotMunIzq2Z = 0;
	}
	// Set up vertex data (and buffer(s)) and attribute pointers
	GLfloat vertices[] =
	{
		// Positions            // Normals              // Texture Coords
		-0.5f, -0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  1.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  0.0f, -1.0f,     1.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    0.0f,  0.0f, -1.0f,     0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  0.0f,
		0.5f, -0.5f,  0.5f,     0.0f,  0.0f,  1.0f,     1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  0.0f,  1.0f,     1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  0.0f,  1.0f,  	1.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,    0.0f,  0.0f,  1.0f,     0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    -1.0f,  0.0f,  0.0f,    1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     0.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     1.0f,  0.0f,  0.0f,     1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  1.0f,
		0.5f, -0.5f,  0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,     0.0f, -1.0f,  0.0f,     1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,    0.0f, -1.0f,  0.0f,     0.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  1.0f,
		0.5f,  0.5f, -0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,     0.0f,  1.0f,  0.0f,     1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,    0.0f,  1.0f,  0.0f,     0.0f,  1.0f
	};

	GLfloat skyboxVertices[] = {
		// Positions
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	GLuint indices[] =
	{  // Note that we start from 0!
		0,1,2,3,
		4,5,6,7,
		8,9,10,11,
		12,13,14,15,
		16,17,18,19,
		20,21,22,23,
		24,25,26,27,
		28,29,30,31,
		32,33,34,35
	};

	// Positions all containers
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,0.0f,0.0f),
		glm::vec3(-3.75f,4.4f,4.45f),
		glm::vec3(0.0f,0.0f,0.0f),
		glm::vec3(0.0f,0.0f,0.0f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	// First, set the container's VAO (and VBO)
	GLuint VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	// Position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)0);
	glEnableVertexAttribArray(0);
	// Normals attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	// Texture Coordinate attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glBindVertexArray(0);
	// Then, we set the light's VAO (VBO stays the same. After all, the vertices are the same for the light object (also a 3D cube))
	GLuint lightVAO;
	glGenVertexArrays(1, &lightVAO);
	glBindVertexArray(lightVAO);
	// We only need to bind to the VBO (to link it with glVertexAttribPointer), no need to fill it; the VBO's data already contains all we need.
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	// Set the vertex attributes (only position data for the lamp))
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid *)0); // Note that we skip over the other data in our buffer object (we don't need the normals/textures, only positions).
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
	//SkyBox
	GLuint skyboxVBO, skyboxVAO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1,&skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices),&skyboxVertices,GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT,GL_FALSE, 3 * sizeof(GLfloat), (GLvoid *)0);
	// Load textures
	vector<const GLchar*> faces;
	faces.push_back("SkyBox/right.tga");
	faces.push_back("SkyBox/left.tga");
	faces.push_back("SkyBox/top.tga");
	faces.push_back("SkyBox/down.tga");
	faces.push_back("SkyBox/back.tga");
	faces.push_back("SkyBox/front.tga");
	GLuint cubemapTexture = TextureLoading::LoadCubemap(faces);
	glm::mat4 projection = glm::perspective(camera.GetZoom(), (GLfloat)SCREEN_WIDTH / (GLfloat)SCREEN_HEIGHT, 0.1f, 1000.0f);
	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Calculate deltatime of current frame
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		DoMovement();
		animacion();
		animacion2();
		animacionC();
		animacionCrist();
		animacionNave();
		animacionCharola();
		animacionTapaCofre();

		// Clear the colorbuffer
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Use cooresponding shader when setting uniforms/drawing objects
		lightingShader.Use();
		GLint viewPosLoc = glGetUniformLocation(lightingShader.Program, "viewPos");
		glUniform3f(viewPosLoc, camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
		// Set material properties
		glUniform1f(glGetUniformLocation(lightingShader.Program, "material.shininess"), 32.0f);
		// == ==========================
		// Here we set all the uniforms for the 5/6 types of lights we have. We have to set them manually and index
		// the proper PointLight struct in the array to set each uniform variable. This can be done more code-friendly
		// by defining light types as classes and set their values in there, or by using a more efficient uniform approach
		// by using 'Uniform buffer objects', but that is something we discuss in the 'Advanced GLSL' tutorial.
		// == ==========================
		// Directional light
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.direction"), -0.2f, -1.0f, -0.3f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.ambient"), 0.4f, 0.4f, 0.4f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.diffuse"), 0.4f, 0.4f, 0.4f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "dirLight.specular"), 0.5f, 0.5f, 0.5f);

		// Point light 1
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].position"), pointLightPositions[0].x, pointLightPositions[0].y, pointLightPositions[0].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].ambient"), 0.05f, 0.05f, 0.05f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].diffuse"), LightP1.x, LightP1.y, LightP1.z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[0].specular"), LightP1.x, LightP1.y, LightP1.z);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[0].quadratic"), 0.032f);

		// Point light 2
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].position"), pointLightPositions[1].x, pointLightPositions[1].y, pointLightPositions[1].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].ambient"), 0.05f, 0.05f, 0.05f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].diffuse"), LightP1.x, LightP1.y, LightP1.z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].specular"), LightP1.x, LightP1.y, LightP1.z);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[1].quadratic"), 0.032f);

		// Point light 3
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].position"), pointLightPositions[2].x, pointLightPositions[2].y, pointLightPositions[2].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[2].ambient"), 0.05f, 0.05f, 0.05f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].diffuse"), LightP1.x, LightP1.y, LightP1.z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].specular"), LightP1.x, LightP1.y, LightP1.z);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[2].quadratic"), 0.032f);

		// Point light 4
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].position"), pointLightPositions[3].x, pointLightPositions[3].y, pointLightPositions[3].z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[3].ambient"), 0.05f, 0.05f, 0.05f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].diffuse"), LightP1.x, LightP1.y, LightP1.z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "pointLights[1].specular"), LightP1.x, LightP1.y, LightP1.z);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "pointLights[3].quadratic"), 0.032f);

		// SpotLight
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.position"), camera.GetPosition().x, camera.GetPosition().y, camera.GetPosition().z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.direction"), camera.GetFront().x, camera.GetFront().y, camera.GetFront().z);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.ambient"), 0.0f, 0.0f, 0.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.diffuse"), 0.0f, 0.0f, 0.0f);
		glUniform3f(glGetUniformLocation(lightingShader.Program, "spotLight.specular"), 0.0f, 0.0f, 0.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.constant"), 1.0f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.linear"), 0.09f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.quadratic"), 0.032f);
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
		glUniform1f(glGetUniformLocation(lightingShader.Program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));

		// Set material properties
		glUniform1f(glGetUniformLocation(lightingShader.Program, "material.shininess"), 32.0f);

		// Create camera transformations
		glm::mat4 view;
		view = camera.GetViewMatrix();
		// Get the uniform locations
		GLint modelLoc = glGetUniformLocation(lightingShader.Program, "model");
		GLint viewLoc = glGetUniformLocation(lightingShader.Program, "view");
		GLint projLoc = glGetUniformLocation(lightingShader.Program, "projection");

		// Pass the matrices to the shader
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

		// Bind diffuse map
		//glBindTexture(GL_TEXTURE_2D, texture1);*/

		// Bind specular map
		/*glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);*/

		glBindVertexArray(VAO);
		glm::mat4 model(1);
		glm::mat4 tmp = glm::mat4(1.0f);
		glm::mat4 tempTio = glm::mat4(1.0f);
		glm::mat4 tempBraIzq = glm::mat4(1.0f);
		glm::mat4 tempBraDer = glm::mat4(1.0f);
		glm::mat4 tempPierIzq = glm::mat4(1.0f);
		glm::mat4 tempPierDer = glm::mat4(1.0f);
		
		glm::mat4 tempWendy = glm::mat4(1.0f);
		glm::mat4 tempBraIzqWendy = glm::mat4(1.0f);
		glm::mat4 tempBraDerWendy = glm::mat4(1.0f);

		//Ubicacion y Dibujado de Modelos-----------------------------------------------------------------------------------------------
		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(5.0f, 0.85f, 5.0f));
		model = glm::translate(model,  glm::vec3(movCarroX, 1.0f, movCarroZ));
		model = glm::rotate(model, glm::radians(rotCarro+rotIniCarro), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		carrito.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(-2.5f, 2.09f, -0.5f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		helado.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, PosIniCristal1 + glm::vec3(0.0f, 0.0f, movCristZ1));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cristal1.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, PosIniCristal2 + glm::vec3(0.0f, 0.0f, movCristZ2));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cristal2.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, PosIniNave + glm::vec3(movNaveX, movNaveY, movNaveZ));
		model = glm::rotate(model, glm::radians(rotNave), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		nave1.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(-1.0f, 2.9f, 3.2f));
		tempTio = model = glm::translate(model, glm::vec3(posX, posY, posZ));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		tio.Draw(lightingShader);

		model = glm::translate(tempTio, glm::vec3(0.3f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotBraIzqX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotBraIzqZ), glm::vec3(0.0f, 0.0f, 1.0f));
		tempBraIzq = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		brazoIzqTio.Draw(lightingShader);

		model = glm::translate(tempBraIzq, glm::vec3(0.0f, -0.15f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunIzqX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunIzqZ), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.01f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		munecaIzqTio.Draw(lightingShader);

		model = glm::translate(tempTio, glm::vec3(-0.3f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotBraDerX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotBraDerZ), glm::vec3(0.0f, 0.0f, 1.0f));
		tempBraDer = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		brazoDerTio.Draw(lightingShader);

		model = glm::translate(tempBraDer, glm::vec3(0.0f, -0.15f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunDerX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunDerZ), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.01f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		munecaDerTio.Draw(lightingShader);

		model = glm::translate(tempTio, glm::vec3(0.09f, -0.45f, 0.05f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotRodIzqX), glm::vec3(1.0f, 0.0f, 0.0f));
		tempPierIzq = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		piernaIzqTio.Draw(lightingShader);

		model = glm::translate(tempPierIzq, glm::vec3(0.0f, -0.35f, 0.0f));
		model = glm::rotate(model, glm::radians(rotPieIzqX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		pieIzqTio.Draw(lightingShader);

		model = glm::translate(tempTio, glm::vec3(-0.09f, -0.45f, 0.05f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotRodDerX), glm::vec3(1.0f, 0.0f, 0.0f));
		tempPierDer = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		piernaDerTio.Draw(lightingShader);

		model = glm::translate(tempPierDer, glm::vec3(0.0f,-0.325f, 0.0f));
		model = glm::rotate(model, glm::radians(rotPieDerX), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::translate(model, glm::vec3(0.0f, 0.05f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		pieDerTio.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(-2.459999f, 2.52f, -2.54f));
		tempWendy = model = glm::translate(model, glm::vec3(pos2X, pos2Y, pos2Z));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		wendy.Draw(lightingShader);

		model = glm::translate(tempWendy, glm::vec3(0.04f, 0.0f, -0.008f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotBraIzq2X), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotBraIzq2Z), glm::vec3(0.0f, 0.0f, 1.0f));
		tempBraIzqWendy = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		braIzqWendy.Draw(lightingShader);

		model = glm::translate(tempBraIzqWendy, glm::vec3(0.09f, -0.13f, -0.01f));
		model = glm::rotate(model, glm::radians(rotMunIzq2X), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunIzq2Z), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.01f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		munIzqWendy.Draw(lightingShader);

		model = glm::translate(tempWendy, glm::vec3(-0.065f, -0.000f, -0.01f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		model = glm::rotate(model, glm::radians(rotBraDer2X), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotBraDer2Z), glm::vec3(0.0f, 0.0f, 1.0f));
		tempBraDerWendy = model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		braDerWendy.Draw(lightingShader);

		model = glm::translate(tempBraDerWendy, glm::vec3(-0.09f, -0.105f, 0.015f));
		model = glm::rotate(model, glm::radians(rotMunDer2X), glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(rotMunDer2Z), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::translate(model, glm::vec3(0.0f, -0.01f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		munDerWendy.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, PosIniCharola + glm::vec3(movCharolaX, 0.0f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		charola.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(-0.1f, 0.925f, 0.0f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		registradora.Draw(lightingShader);

		model = glm::mat4(1);
		tmp = model = glm::translate(model, glm::vec3(-2.16f, 2.05f, -5.1f));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cofre.Draw(lightingShader);

		model = glm::mat4(1);
		tmp=model = glm::translate(tmp, glm::vec3(0.0f, 0.1f, -0.18f));
		model = glm::rotate(tmp, glm::radians(rotTapaCofre), glm::vec3(1.0f, 0.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		tapaCofre.Draw(lightingShader);
		
		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0.0f, 1.08f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		suelo.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0.0f, 1.08f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		waddles.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0.0f, 1.08f, 0.0f));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		dipper.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cabana.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		mueble.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		barril.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		lampara.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		mabel.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		bill.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		maquina.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		pie.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		casco.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		tumbas.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		nave2.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		nave3.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cuadernos.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		cubo.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		escritorio.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		pez.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		reloj.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		unicornio.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		chetos.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		silla.Draw(lightingShader);

		model = glm::mat4(1);
		model = glm::translate(model, glm::vec3(0, 1, 0));
		model = glm::rotate(model, glm::radians(rot), glm::vec3(0.0f, 1.0f, 0.0));
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		alfombra.Draw(lightingShader);

		glBindVertexArray(0);
		// Also draw the lamp object, again binding the appropriate shader
		lampShader.Use();
		// Get location objects for the matrices on the lamp shader (these could be different on a different shader)
		modelLoc = glGetUniformLocation(lampShader.Program, "model");
		viewLoc = glGetUniformLocation(lampShader.Program, "view");
		projLoc = glGetUniformLocation(lampShader.Program, "projection");
		// Set matrices
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		model = glm::mat4(1);
		model = glm::translate(model, lightPos);
		//model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		// Draw the light object (using light's vertex attributes)
		glBindVertexArray(lightVAO);
		for (GLuint i = 0; i < 4; i++)
		{
			model = glm::mat4(1);
			model = glm::translate(model, pointLightPositions[i]);
			model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube
			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		glBindVertexArray(0);
		// Draw skybox as last
		glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
		SkyBoxshader.Use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix()));	// Remove any translation component of the view matrix
		glUniformMatrix4fv(glGetUniformLocation(SkyBoxshader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(SkyBoxshader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		// skybox cube
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // Set depth function back to default
		// Swap the screen buffers
		glfwSwapBuffers(window);
	}
	glDeleteVertexArrays(1, &VAO);
	glDeleteVertexArrays(1, &lightVAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	glDeleteVertexArrays(1, &skyboxVAO);
	glDeleteBuffers(1, &skyboxVBO);
	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

//ANIMACION EJERCIONES SIMPLES------------------------------------------------------------------------------------------------------------
//Funcion que realiza la animacion por Keyframes para el personaje tio
void animacion()
{
	//Movimiento del personaje
	if (play)
	{
		if (i_curr_steps >= i_max_steps) //end of animation between frames?
		{
			playIndex++;
			if (playIndex>FrameIndex - 2)	//end of total animation?
			{
				printf("\ntermina anim");
				playIndex = 0;
				play = false;
			}
			else //Next frame interpolations
			{
				i_curr_steps = 0; //Reset counter
									//Interpolation
				interpolation();
			}
		}
		else
		{
			//Draw animation
			posX += KeyFrame[playIndex].incX;
			posY += KeyFrame[playIndex].incY;
			posZ += KeyFrame[playIndex].incZ;

			rotRodIzqX += KeyFrame[playIndex].rotIncRodIzqX;
			rotRodDerX += KeyFrame[playIndex].rotIncRodDerX;
			rotPieIzqX += KeyFrame[playIndex].rotIncPieIzqX;
			rotPieDerX += KeyFrame[playIndex].rotIncPieDerX;
			rotBraIzqX += KeyFrame[playIndex].rotIncBraIzqX;
			rotBraDerX += KeyFrame[playIndex].rotIncBraDerX;
			rotMunIzqX += KeyFrame[playIndex].rotIncMunIzqX;
			rotMunDerX += KeyFrame[playIndex].rotIncMunDerX;

			rotBraIzqZ += KeyFrame[playIndex].rotIncBraIzqZ;
			rotBraDerZ += KeyFrame[playIndex].rotIncBraDerZ;
			rotMunIzqZ += KeyFrame[playIndex].rotIncMunIzqZ;
			rotMunDerZ += KeyFrame[playIndex].rotIncMunDerZ;
			i_curr_steps++;
		}

	}
}

//Funcion que realiza la animacion para el personaje Wendy
void animacion2()
{
	//Movimiento del personaje
	if (play2)
	{
		if (i_curr_steps2 >= i_max_steps2) //end of animation between frames?
		{
			playIndex2++;
			if (playIndex2 > FrameIndex2 - 2)	//end of total animation?
			{
				printf("\ntermina anim");
				playIndex2 = 0;
				play2 = false;
			}
			else //Next frame interpolations
			{
				i_curr_steps2 = 0; //Reset counter
									//Interpolation
				interpolation2();
			}
		}
		else
		{
			//Draw animation
			pos2X += KeyFrame2[playIndex2].inc2X;
			pos2Y += KeyFrame2[playIndex2].inc2Y;
			pos2Z += KeyFrame2[playIndex2].inc2Z;

			rotBraIzq2X += KeyFrame2[playIndex2].rotIncBraIzq2X;
			rotBraDer2X += KeyFrame2[playIndex2].rotIncBraDer2X;
			rotMunIzq2X += KeyFrame2[playIndex2].rotIncMunIzq2X;
			rotMunDer2X += KeyFrame2[playIndex2].rotIncMunDer2X;

			rotBraIzq2Z += KeyFrame2[playIndex2].rotIncBraIzq2Z;
			rotBraDer2Z += KeyFrame2[playIndex2].rotIncBraDer2Z;
			rotMunIzq2Z += KeyFrame2[playIndex2].rotIncMunIzq2Z;
			rotMunDer2Z += KeyFrame2[playIndex2].rotIncMunDer2Z;
			i_curr_steps2++;
		}

	}
}

//ANIMACION EJERCIONES SIMPLES------------------------------------------------------------------------------------------------------------
//Funcion que realiza el movimiento del carrito de golf
void animacionC()
{
	if (animCarro)
	{
		if (recorrido1)
		{
			rotCarro = 0;
			movCarroX += 0.1f;
			if (movCarroX > 10)
			{
				recorrido1 = false;
				recorrido2 = true;
			}
		}
		if (recorrido2)
		{
			rotCarro = -90;
			movCarroZ += 0.1f;
			if (movCarroZ > 5)
			{
				recorrido2 = false;
				recorrido3 = true;
			}
		}
		if (recorrido3)
		{
			rotCarro = 180;
			movCarroX -= 0.1f;
			if (movCarroX < 0)
			{
				recorrido3 = false;
				recorrido4 = true;
			}
		}
		if (recorrido4)
		{
			rotCarro = 90;
			movCarroZ -= 0.1f;
			if (movCarroZ < 0)
			{
				recorrido4 = false;
				recorrido1 = true;
			}
		}
	}
}

//Funcion que realiza el movimiento de los cristales para abrir la heladera
void animacionCrist()
{
	//Movimiento del coche
	if (animCristal)
	{
		if (recoCrist1)
		{
			movCristZ1 += 0.006f;
			if (movCristZ1 > 0.5)
			{
				recoCrist1 = false;
				recoCrist2 = true;
			}
		}
		if (recoCrist2)
		{
			movCristZ1 -= 0.006f;
			if (movCristZ1 < 0)
			{
				recoCrist2 = false;
				recoCrist3 = true;
			}
		}
		if (recoCrist3)
		{
			movCristZ2 -= 0.006f;
			if (movCristZ2 < -0.5)
			{
				recoCrist3 = false;
				recoCrist4 = true;
			}
		}
		if (recoCrist4)
		{
			movCristZ2 += 0.006f;
			if (movCristZ2 > 0)
			{
				recoCrist4 = false;
				recoCrist1 = true;
			}
		}
	}
}

//Funcion que realiza el movimiento de la nave al rededor de la cabaña
void animacionNave()
{
	if (animNave)
	{
		if (recoNave1)
		{
			rotNave = 0.0;
			movNaveX -= 0.6f;
			movNaveY -= 0.6f;
			if (movNaveX <-20)
			{
				recoNave1 = false;
				recoNave2 = true;
			}
		}
		if (recoNave2)
		{
			rotNave = 0.0;
			movNaveX -= 0.6f;
			if (movNaveX < -50)
			{
				recoNave2 = false;
				recoNave3 = true;
			}
		}
		if (recoNave3)
		{
			rotNave = 90.0;
			movNaveZ += 0.6f;
			if (movNaveZ > 30)
			{
				recoNave3 = false;
				recoNave4 = true;
			}
		}
		if (recoNave4)
		{
			rotNave = 180.0;
			movNaveX += 0.6f;
			if (movNaveX > -20)
			{
				recoNave4 = false;
				recoNave5 = true;
			}
		}
		if (recoNave5)
		{
			rotNave = 180.0;
			movNaveX += 0.6f;
			movNaveY += 0.6f;
			if (movNaveY > 20)
			{
				recoNave5 = false;
				recoNave6 = true;
			}
		}
		if (recoNave6)
		{
			rotNave = 270.0;
			movNaveZ -= 0.6f;
			if (movNaveZ < 0)
			{
				recoNave6 = false;
				recoNave1 = true;
			}
		}
	}
}

//Funcion que realiza el movimiento de abrir y cerrar la charola de la caja registradora
void animacionCharola()
{
	if (animCharola)
	{
		if (recoCharola1)
		{
			movCharolaX += 0.001f;
			if (movCharolaX > 0.145)
			{
				recoCharola1 = false;
				recoCharola2 = true;
			}
		}
		if (recoCharola2)
		{
			movCharolaX -= 0.001f;
			if (movCharolaX < -0.05)
			{
				recoCharola2 = false;
				recoCharola1 = true;
			}
		}
	}
}

//Funcion que realiza el movimiento de abrir y cerrar el cofre del tesoro
void animacionTapaCofre()
{
	if (animTapaCofre)
	{
		if (recoTapaCofre1)
		{
			rotTapaCofre += 0.6f;
			if (rotTapaCofre > 140.0)
			{
				recoTapaCofre1 = false;
				recoTapaCofre2 = true;
			}
		}
		if (recoTapaCofre2)
		{
			rotTapaCofre -= 0.6f;
			if (rotTapaCofre < 0)
			{
				recoTapaCofre2 = false;
				recoTapaCofre1 = true;
			}
		}
	}
}

// Is called whenever a key is pressed/released via GLFW
void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode)
{
	//Activa Animaciones por Keyframes----------------------------------------------------------------------------------------------------------------------------
	if (keys[GLFW_KEY_K])
	{
		if (play == false && (FrameIndex > 1))
		{
			//Reinicia la animacion
			resetElements();
			//First Interpolation				
			interpolation();

			play = true;
			playIndex = 0;
			i_curr_steps = 0;
		}
		else
		{
			play = false;
		}
	}

	if (keys[GLFW_KEY_B])
	{
		if (play2 == false && (FrameIndex2 > 1))
		{

			resetElements2();
			//First Interpolation				
			interpolation2();

			play2 = true;
			playIndex2 = 0;
			i_curr_steps2 = 0;
		}
		else
		{
			play2 = false;
		}
	}
	//Carga Animaciones por Keyframes
	if (keys[GLFW_KEY_L])
	{
		if (FrameIndex<MAX_FRAMES)
		{
			cargaFrames();
		}
	}
	if (keys[GLFW_KEY_N])
	{
		if (FrameIndex2 < MAX_FRAMES2)
		{
			cargaFrames2();
		}
	}
	if (GLFW_KEY_ESCAPE == key && GLFW_PRESS == action)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			keys[key] = false;
		}
	}
	//Activa o Desactiva Luces
	if (keys[GLFW_KEY_SPACE])
	{
		active = !active;
		if (active) {
			LightP1 = glm::vec3(1.0f, 0.0f, 0.0f);
		}
		else {
			LightP1 = glm::vec3(0.0f, 0.0f, 0.0f);
		}
	}
}

void MouseCallback(GLFWwindow *window, double xPos, double yPos)
{
	if (firstMouse)
	{
		lastX = xPos;
		lastY = yPos;
		firstMouse = false;
	}
	GLfloat xOffset = xPos - lastX;
	GLfloat yOffset = lastY - yPos;  // Reversed since y-coordinates go from bottom to left
	lastX = xPos;
	lastY = yPos;
	camera.ProcessMouseMovement(xOffset, yOffset);
}

// Moves/alters the camera positions based on user input
void DoMovement()
{
	//Activa o Desactiva Animaciones Simples-------------------------------------------------------------------------------------------------------
	if (keys[GLFW_KEY_R])
	{
		animCarro = true;
	}
	if (keys[GLFW_KEY_T])
	{
		animCarro = false;
	}
	if (keys[GLFW_KEY_F])
	{
		animCristal = true;
	}
	if (keys[GLFW_KEY_G])
	{
		animCristal = false;
	}
	if (keys[GLFW_KEY_Y])
	{
		animNave = true;
	}
	if (keys[GLFW_KEY_U])
	{
		animNave = false;
	}
	if (keys[GLFW_KEY_H])
	{
		animCharola = true;
	}
	if (keys[GLFW_KEY_J])
	{
		animCharola = false;
	}
	if (keys[GLFW_KEY_I])
	{
		animTapaCofre = true;
	}
	if (keys[GLFW_KEY_O])
	{
		animTapaCofre = false;
	}

	// Camera controls
	if (keys[GLFW_KEY_W] || keys[GLFW_KEY_UP])
	{
		camera.ProcessKeyboard(FORWARD, deltaTime);

	}
	if (keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN])
	{
		camera.ProcessKeyboard(BACKWARD, deltaTime);


	}
	if (keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT])
	{
		camera.ProcessKeyboard(LEFT, deltaTime);


	}
	if (keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT])
	{
		camera.ProcessKeyboard(RIGHT, deltaTime);
	}
}