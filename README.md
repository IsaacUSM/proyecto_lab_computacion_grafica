<p align="center">
  
  <a href="https://imgur.com/k4FiRqg" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/k4FiRqg.png" alt="Project logo"></a>
 
</p>
<h3 align="center">Gravity Falls</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center">
    Recreación de la cabaña del misterio con animaciones simples y complejas(keyframes), múltiples modelos y personajes de la serie mencionada. <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

Proyecto final para el Laboratorio de la materia de Computación Gráfica e Interacción Humano-Computadora.<br>
Tarda un poco en abrir por favor ten paciencia.<br>

## 🏁 Getting Started <a name = "getting_started"></a>

$ git clone git@gitlab.com:IsaacUSM/proyecto_lab_computacion_grafica.git

### Prerequisites

Visual Studio.

### Installing

Paso 1: Ve a la página de Visual Studio en "https://visualstudio.microsoft.com/es/" y descarga el instalador de Community 2019.<br>

Paso 2: Haz clic en Visual Studio Community 2019 e instala.<br>

Paso 3: Elige qué software adicional quieres descargar y haz clic en Install. La instalación se iniciará.<br>

Paso 4: Una vez instalado el software, verás la pantalla de inicio de sesión. Inicia sesión o crea una cuenta si aún no la tienes. También puedes optar por hacerlo más tarde haciendo clic en 'Not now, maybe later'.<br>

Paso 5: Elige un tema y haz clic en 'Start Visual Studio'.<br>

## 🔧 Running the tests <a name = "tests"></a>


Opción 1<br>

Paso 1: Abrir el proyecto o la solución en Visual Studio.<br>

Paso 2: Hacer clic en Depurador local de Windows.<br>

Opción 2<br>

Paso 1: Entrar a la carpeta Debug que tiene la ruta siguiente "Gravity_Falls\Debug".<br>

Paso 2: Abrir el archivo ejecutable llamado "Gravity_Falls.exe".<br>

## 🎈 Usage <a name="usage"></a>

Desplazamiento con la cámara utilizando las teclas: <br>
A = "Izquierda"<br>
S = "Atrás"<br>
D = "Derecha"<br>
W = "Adelante"<br>
Activación de animaciones utilizando las teclas:<br>
R = "Activa animación de Carrito de Golf"<br>
T = "Detiene animación de Carrito de Golf"<br>
Y = "Activa animación de Nave Espacial"<br>
U = "Detiene animación de Nave Espacial"<br>
I = "Activa animación de Cofre Del Tesoro"<br>
O = "Detiene animación de Cofre Del Tesoro"<br>
F = "Activa animación de Congelador"<br>
G = "Detiene animación de Congelador"<br>
H = "Activa animación de Caja Registradora"<br>
J = "Detiene animación de Caja Registradora"<br>
K = "Activa animación de Personaje Tío"<br>
L = "Carga animación de Personaje Tío"<br>
B = "Activa animación de Personaje Wendy"<br>
N = "Carga animación de Personaje Wendy"<br>
Activación de luces utilizando la tecla:<br>
Barra espaciadora = "Activa/Desactiva luz"<br>
Cerrar la ejecución del software utiliza la tecla:<br>
ESC = "Cierra y detiene la ejecución del software"<br>

Para mas información revisar los manuales de usuario y técnico.<br>

## ⛏️ Built Using <a name = "built_using"></a>

- [Visual Studio](https://visualstudio.microsoft.com/es/) - IDE

## ✍️ Authors <a name = "authors"></a>

- [@IsaacUSM](https://gitlab.com/IsaacUSM) - Idea & Initial work

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Me gustaría dar las gracias a todos los que han utilizado el código<br>
- Inspiración<br>
  Serie Animada Gravity Falls
